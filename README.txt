README
------

For quite a long time, Drupal has had several "friends APIs". Buddylist,
Buddylist2, Friend, Ajax Friend, User Relationships, FriendList, etc.

This made it _really_ hard for module developers to just ask: "are user A
and user B friends...?" Each module had a different way of answering that
question, and with different outcomes.

This API is meant to be the answer to this problem.

It provides a general API which answers the most important questions about
user relations. Who are user A's friends? Is user A  friends with User B? What
relation types are available?


