README.developers.txt
---------------------

* Are you the maintainer of a module that sets user relations?

This module is a way to give other modules a standard way to find out
about relations.
The best thing to do is to take  user_relations_api.api.default.inc, copy
it onto  user_relations_api.api.YOUR_MODULE.inc, and just implement those
functions.

* Are the the maintainer of a module that needs to know who is related to who?

If that is the case, just have a look at user_relations_api.module. There are
a bunch of well documented functions that will allow you to discover who
is related to who and how, without worrying about the underlying "relations"
module.
Note that the rid at the end is optional. This means that if you don't set it,
your module will get back whatever default they set in
Administer -> Settings -> User Relations API.

