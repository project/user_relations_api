<?php


/**
 * @file
 * Relation API, "friendlist" implementation
 *
 * To see what each function does, please see user_relations_api.module where
 * these functions are fully documented
 */

###########################################################
# Relation types
###########################################################

function _user_relations_api_relation_types_all(){

  $types = friendlist_api_relation_types_load_all();

  // Accommodate fields as required by the API
  foreach($types as $type){
    _relation_api_fix_type($type);
  }

  return $types;

}

function _user_relations_api_relation_type($rtid){
  $type = friendlist_api_relation_type_load($rtid); 
  // Accommodate fields as required by the API
  _relation_api_fix_type($type);

  return $type;

}

function _relation_api_fix_type(&$type){
  $type->SOMETHING = 10;  

}

###########################################################
# Two way relations
###########################################################

function _user_relations_api_tw_is_active($user1, $user2, $rtid){
  return friendlist_api_relation_status_get($user1, $user2, $rtid) == 'TW_BOTH';
}

function _user_relations_api_tw_is_pending($user1, $user2, $rtid){
  return friendlist_api_relation_status_get($user1, $user2, $rtid) == 'TW_1_TO_2_P';
}


function _user_relations_api_tw_all_pending_from($account, $rtid){
  return friendlist_api_db_statuses_array($account, NULL, $rtid, 'TW_1_TO_2_P');
}

function _user_relations_api_tw_all_pending_to($account, $rtid){
  return friendlist_api_db_statuses_array(NULL, $account, $rtid, 'TW_1_TO_2_P','requester_id');
}

function _user_relations_api_tw_all($account, $rtid){
  return friendlist_api_db_statuses_array($account, NULL, $rtid, 'TW_BOTH');
  
}

###########################################################
# One way relations
###########################################################

function _user_relations_api_ow_is_active($user1, $user2, $rtid){
  $status = friendlist_api_relation_status_get($user1, $user2, $rtid);
  return ($status  == 'OW_BOTH' || $status == 'OW_1_TO_2' );
}

function _user_relations_api_ow_all_pending_from($account, $rtid){
  // Not implemented in FriendList. Function always empty
  return array();
}
function _user_relations_api_ow_all_pending_to($account, $rtid){
  // Not implemented in FriendList. Function always empty
  return array();
}

function _user_relations_api_ow_all_from($account, $rtid){
  return friendlist_api_db_statuses_array($account, 
                                          NULL, 
                                          $rtid, 
                                          array('OW_BOTH' , 'OW_1_TO_2')
                                         );
}
function _user_relations_api_ow_all_to($account, $rtid){
  return friendlist_api_db_statuses_array(NULL, 
                                          $account, 
                                          $rtid, 
                                          array('OW_BOTH','OW_1_TO_2'),
                                          'requester_id'
                                         );
}

#drupal_set_message("HERE: ".serialize(_user_relations_api_relation_types_all() ) );
#drupal_set_message("HERE: ".serialize(user_relations_api_tw_all(1) ) );

?>
