<?php

/**
 * @file
 * Relation API, "user_relationships" implementation
 *
 * To see what each function does, please see user_relations_api.module where
 * these functions are fully documented
 */


###########################################################
# Relation types
###########################################################

function _user_relations_api_relation_types_all(){
  return array();
}

function _user_relations_api_relation_type($rtid){
  return new stdClass();
}

###########################################################
# Two way relations
###########################################################

function _user_relations_api_tw_is_active($user1, $user2, $rtid){
  return array();
}

function _user_relations_api_tw_is_pending($user1, $user2, $rtid){
  return array();
}


function _user_relations_api_tw_all_pending_from($account, $rtid){
  return array();
}

function _user_relations_api_tw_all_pending_to($account, $rtid){
  return array();
}

function _user_relations_api_tw_all($account, $rtid){
  return array();
}

###########################################################
# One way relations
###########################################################

function _user_relations_api_ow_is_active($user1, $user2, $rtid){
  return array();
}

function _user_relations_api_ow_all_pending_from($account, $rtid){
  return array();
}
function _user_relations_api_ow_all_pending_to($account, $rtid){
  return array();
}

function _user_relations_api_ow_all_from($account, $rtid){
  return array();
}
function _user_relations_api_ow_all_to($account, $rtid){
  return array();
}


?>
