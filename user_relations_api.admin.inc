<?php

/**
 * @file
 * user_relations_api.admin module file
 *
 */

/**
 * Return the configuration form
 *
 * @return
 *   The config form
 */
function user_relations_api_admin_settings() {

  $options['default'] = 'None';

  // Add the supported modules to the list
  if (module_exists('friendlist_api')){
    $options['friendlist'] = 'Friendlist';
  }
  if (module_exists('user_relationships') ){
    $options['user_relationships'] = 'User Relationships';
  }

  $form['user_relations_api_api'] = array(
      '#title' => t('Maximum number of items that can be grouped'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => variable_get('user_relations_api_api','default')
    );
  $form['user_relations_api_rtid_ow'] = array(
      '#title' => t('Default relation type ID (one way relations)'),
      '#type' => 'textfield',
      '#size' => 8,
      '#default_value' => variable_get('user_relations_api_rtid_ow','0')
    );
  $form['user_relations_api_rtid_tw'] = array(
      '#title' => t('Default relation type ID (two way relations)'),
      '#type' => 'textfield',
      '#size' => 8,
      '#default_value' => variable_get('user_relations_api_rtid_tw','0')
    );


  return system_settings_form($form);
}

